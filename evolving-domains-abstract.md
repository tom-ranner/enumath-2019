---
author:
- name: Thomas Ranner
  address: School of Computing, University of Leeds, UK
  email: T.Ranner@leeds.ac.uk
title: A unified  theory for continuous in time evolving finite element space approximations to partial differential equations in evolving domains
date: 
documentclass: amsart
bibliography: library.bib
link-citations: true
---
## Abstract ##

We develop a unified theory for continuous in time  finite element discretisations of partial differential equations posed in evolving domains including  the consideration of equations posed on evolving surfaces and bulk domains as well coupled surface bulk systems.
We use an abstract variational setting with time dependent function spaces and abstract time dependent finite element spaces.
Optimal a priori bounds are shown under usual assumptions on perturbations of bilinear forms and approximation properties of the abstract finite element spaces.
The abstract theory is applied to evolving finite elements in both flat and curved spaces.
The theory allows us to give precise definitions which relate the abstract theory to concrete constructions and show which assumptions lead to stability and error estimates.
Our approach allows an isoparametric approximation of parabolic equations in general domains.
Numerical experiments are described which confirm the rates of convergence.

This talk describes some of the work in [@EllRan17-pp].

![Example of the isoparametric construction we use for the case of a evolving curve embedded in $\mathbb{R}^2$.](./surf-domain-construction2.pdf)

## Funding ##

This work was supported by a Leverhulme Trust early career fellowship.

## References ##

<!--  LocalWords:  isoparametric
 -->
