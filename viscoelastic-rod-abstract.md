---
author:
- name: Thomas Ranner
  address: School of Computing, University of Leeds, UK
  email: T.Ranner@leeds.ac.uk
title: A stable finite element method for an open, inextensible, viscoelastic rod with applications to nematode locomotion
date: 
documentclass: amsart
bibliography: library.bib
link-citations: true
---
## Abstract ##

We present and analyse a numerical method for understanding the dynamics of an open, inextensible viscoelastic rod - a long and thin three dimensional object. Our model allows for both elastic and viscous, bending and twisting deformations and describes the evolution of the midline curve of the rod as well as an orthonormal frame which full determines the rod's three dimensional geometry.
The numerical method is based on using a combination of piecewise linear and piecewise constant functions based on a novel rewriting of the model equations. We derive a stability estimate for the semi-discrete scheme and show that at the fully discrete level that we have good control over the length element and preserve the frame orthonormality conditions up to machine precision. Numerical experiments demonstrate both the good properties of the method as well as the applicability of the method for simulating locomotion of the microscopic nematode Caenorhabditis elegans.

This talk describes the work available in the preprint [@Ranner2019-pp].

![A cartoon of a conformation of a rod. The image shows a shaded three dimensional region which can be parametrized by a midline curve and an orthogonal triad of vectors.](./3d-geometry.png){ width=50% }

## Funding ##

This work was supported by a Leverhulme Trust early career fellowship.

## References ##

<!--  LocalWords:  discretisations PDEs viscoelastic Ranner midline
 -->
<!--  LocalWords:  orthonormality elegans
 -->
