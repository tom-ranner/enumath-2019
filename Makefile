PDF_TARGETS=evolving-domains-abstract.pdf \
     viscoelastic-rod-abstract.pdf \

HTML_TARGETS=evolving-domains.html \
     viscoelastic-rod.html \
     evolving-domains.embed.html \
     viscoelastic-rod.embed.html \
     evolving-domains.full.html \
     viscoelastic-rod.full.html

TARGETS=${PDF_TARGETS} ${HTML_TARGETS}

BUILDDIR=public

all: ${TARGETS}

%.pdf: %.md library.bib
	pandoc --filter pandoc-citeproc \
               --template=./latex-template.tex \
               -s $< -o $@

%.html: %.md library.bib
	pandoc --filter pandoc-citeproc \
	--mathjax \
	--template=./revealjs-template.html \
	-t revealjs \
	--slide-level 2 \
	-V controls=true \
	-V controlsTutorial=true \
	-V slideNumber="'c'" \
	-s $< -o $@

%.embed.html: %.md library.bib
	pandoc --filter pandoc-citeproc \
	--mathjax \
	--template=./revealjs-template.html \
	-t revealjs \
	-V controls=true \
	-V controlsTutorial=true \
	-V slideNumber=false \
	--slide-level 2 \
	-V embedded="true" \
	-s $< -o $@

%.full.html: %.md library.bib
	pandoc --filter pandoc-citeproc \
	--self-contained \
	--mathjax \
	--template=./revealjs-template.html \
	-t revealjs \
	--slide-level 2 \
	-V controls=false \
	-V slideNumber="'c'" \
	-s $< -o $@

build: ${HTML_TARGETS}
	mkdir -p ${BUILDDIR}
	cp ${HTML_TARGETS} ${BUILDDIR}/.
	cp -r css ${BUILDDIR}/css
	cp -r img ${BUILDDIR}/img
	cp -r js ${BUILDDIR}/js
	cp -r video ${BUILDDIR}/video

clean:
	rm -f ${TARGETS}
	rm -rf ${BUILDDIR}
